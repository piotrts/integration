(ns integration.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [datomic.api :as d]
            [datomic-schema.schema :as s]
            [hiccup.page :refer [include-js include-css html5]]
            [integration.middleware :refer [wrap-middleware]]
            [com.kaicode.wocket.server :as ws]
            [com.kaicode.dsync :as dsync]
            [com.kaicode.dsync.db :as db]
            [config.core :refer [env]]))

(def uri "datomic:mem://foksal-integration")

(d/delete-database uri)
(d/create-database uri)

(def conn (d/connect uri))

(def schema
  [(s/schema event
    (s/fields
     [type :long]; :enum [:default :meeting]]
     [start :instant]
     [end :instant]
     [title :string]
     [desc :string]))])

(d/transact conn
  (s/generate-schema schema))

;; I know, this is to put config into schema.edn to make my life easier
(defn spit-env [conn]
  (spit "resources/schema.edn"
        {:db-url uri
         :datomic-schema (s/generate-schema schema)}))

(spit-env conn)

(defonce done? (atom false))

(println "transact")

(def t
  (d/transact conn
    [{:db/id (d/tempid :db.part/user)
      :event/type 1;:nox/default
      :event/start #inst "2017-03-05T18:00:00.000-00:00"
      :event/end #inst "2017-03-12T00:00:00.000-00:00"
      :event/desc "An event that takes about a week"}
     {:db/id  (d/tempid :db.part/user)
      :event/type 1;:nox/default
      :event/start #inst "2017-03-06T18:00:00.000-00:00"
      :event/end #inst "2017-03-07T18:00:00.000-00:00"
      :event/desc "An event that takes 24 hours and spands 2 days"}
     {:db/id  (d/tempid :db.part/user)
      :event/type 1;:nox/default
      :event/start #inst "2017-03-01T02:00:00.000-00:00"
      :event/end #inst "2017-03-02T10:00:00.000-00:00"
      :event/desc "An event that takes 32 hours"}
     ;; -------
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-03-31T01:00:00.000+02:00"
      :event/end #inst "2017-03-31T10:00:00.000+02:00"
      :event/desc "Night"
      :event/title "Sleep time #1. Need more sleep."}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-03-29T01:00:00.000+02:00"
      :event/end #inst "2017-03-29T10:00:00.000+02:00"
      :event/desc "Night"
      :event/title "Sleep time #2. Need more sleep."}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T01:00:00.000+02:00"
      :event/end #inst "2017-04-06T03:00:00.000+02:00"
      :event/desc "some event that start and ends quite early."
      :event/title "Supper time."}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T01:05:00.000+02:00"
      :event/end #inst "2017-04-06T03:05:00.000+02:00"
      :event/desc "(2017-04-06 :: 01:05 - 03:05)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T07:45:00.000+02:00"
      :event/end #inst "2017-04-06T10:55:00.000+02:00"
      :event/desc "(2017-04-06 :: 07:45 - 10:55)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T08:40:00.000+02:00"
      :event/end #inst "2017-04-06T12:50:00.000+02:00"
      :event/desc "(2017-04-06 :: 08:40 - 12:50)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T06:00:00.000+02:00"
      :event/end #inst "2017-04-06T23:35:00.000+02:00"
      :event/desc "(2017-04-06 :: 06:00 - 23:35)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T13:00:00.000+02:00"
      :event/end #inst "2017-04-06T15:30:00.000+02:00"
      :event/desc "(2017-04-06 :: 13:00 - 15:30)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T15:00:00.000+02:00"
      :event/end #inst "2017-04-06T18:00:00.000+02:00"
      :event/desc "(2017-04-06 :: 15:00 - 18:00)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T20:00:00.000+02:00"
      :event/end #inst "2017-04-06T22:00:00.000+02:00"
      :event/desc "(2017-04-06 :: 20:00 - 22:00)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T21:00:00.000+02:00"
      :event/end #inst "2017-04-06T23:00:00.000+02:00"
      :event/desc "(2017-04-06 :: 21:00 - 23:00)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T21:05:00.000+02:00"
      :event/end #inst "2017-04-06T23:05:00.000+02:00"
      :event/desc "(2017-04-06 :: 21:05 - 23:05)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T05:00:00.000+02:00"
      :event/end #inst "2017-04-06T11:00:00.000+02:00"
      :event/desc "(2017-04-06 :: 05:00 - 11:00)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T17:00:00.000+02:00"
      :event/end #inst "2017-04-06T23:30:00.000+02:00"
      :event/desc "(2017-04-06 :: 17:00 - 23:30)"}
     {:db/id   (d/tempid :db.part/user)
      :event/type 1 ;:event.type/default
      :event/start #inst "2017-04-06T23:00:00.000+02:00"
      :event/end #inst "2017-04-06T23:55:00.000+02:00"
      :event/desc "(2017-04-06 :: 23:00 - 23:55)"}]))

(def mount-target
  [:div#app
      [:h3 "ClojureScript has not been compiled!"]
      [:p "please run "
       [:b "lein figwheel"]
       " in order to start the compiler"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   [:link {:rel "stylesheet" :href "https://code.getmdl.io/1.2.0/material.grey-pink.min.css"}]
   [:script {:src "https://storage.googleapis.com/code.getmdl.io/1.2.0/material.min.js"}]
   [:link {:rel "stylesheet" :href "https://fonts.googleapis.com/icon?family=Material+Icons"}]

   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))])

(defn loading-page []
  (html5
    (head)
    [:body {:class "body-container"}
     mount-target
     (include-js "/js/app.js")]))


(defroutes routes
  (GET "/" [] (loading-page))
  (GET "/about" [] (loading-page))
  (GET "/ws" [] ws/listen-for-client-websocket-connections)
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
