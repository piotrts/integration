(ns integration.server
  (:require [integration.handler :refer [app]]
            [config.core :refer [env]]
            [org.httpkit.server :only [run-server]])
  (:gen-class))

 (defn -main [& args]
   (let [port (Integer/parseInt (or (env :port) "3000"))]
     (run-server app {:port port :join? false})))
