(ns integration.core
  (:require-macros  [cljs.core.async.macros :refer [go go-loop]])
  (:require [reagent.core :as r ]
            [reagent.session :as session]
            [clojure.core.async :refer [chan close! timeout put! take! <! >!]]
            [com.kaicode.dsync :as dsync]
            [mount.core :as mount]
            [com.kaicode.dsync.db :as db]
            [com.kaicode.nox.core :as nox]
            [com.kaicode.nox.db :as nox.db]
            [datascript.core :as d]
            [secretary.core :as secretary :include-macros true]
            [accountant.core :as accountant]))

(db/when-ds-ready
  (fn [_]
    (set! nox.db/*transact-fn* (fn [& args]
                                 (apply dsync/transact (rest args))))))

(defn- data-loop [app-state]
  (dsync/data-loop
    {:datomic-query-n-params 
     '[[:find [(pull ?e [*]) ...]
        :where [?e :event/type]]]
     :on-data-available
     (fn [x]
       (js/console.log "got" (pr-str x))
       x)}))


; -------------------------
;; Views

(defonce app-state (r/atom {}))

(defn ^:export load-css [path]
  (let [link (.createElement js/document "link")]
    (doto link
      (.setAttribute "rel" "stylesheet")
      (.setAttribute "type" "text/css")
      (.setAttribute "href" path))
    (.appendChild js/document.head link)))

(defn home-page []
  (data-loop app-state)
  (set! nox.db/*conn* db/conn)
  (load-css "css/nox-style.css")
  [nox/component app-state])

(defn current-page []
  [:div [(session/get :current-page)]])


;; -------------------------
;; Routes

(secretary/defroute "/" []
  (session/put! :current-page #'home-page))

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount/start)
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (db/when-ds-ready
    (fn []
      (mount-root)))) 

(init!)

